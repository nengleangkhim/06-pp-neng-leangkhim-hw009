import logo from './logo.svg';
import './App.css';
import React, { Component } from 'react'
import NavbarLink from './components/NavbarLink';
import Home from './components/home'
import Video from './components/video'
import Account from './components/account'
import Auth from './components/auth'
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";
import { Container } from 'react-bootstrap';
import Welcome from './components/welcome';

export default function App() {
    return (
        <Router>            
            <div>
                <NavbarLink/>
                <Container>
                    <Switch>
                        <Route exact path="/home" component={Home}/>
                        <Route path="/video" component={Video}/>
                        <Route path="/account" component={Account}/>
                        <Route path="/welcome" component={Welcome}/>
                        <Route path="/auth"  component={Auth}/>

                    </Switch>
                </Container>
            </div>
        </Router>
    )
}
