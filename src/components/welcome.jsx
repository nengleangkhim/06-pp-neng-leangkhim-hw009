import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
  import { Button,NavLink,Nav } from 'react-bootstrap'
import Auth from './auth'

export default function welcome() {
    return (
        <div>
            <h1>Welcome</h1>
            <Link to="/auth" className="anima">
                <Button variant="secondary" size="md" >  
                        Logout           
                </Button>
            </Link>  
            
        </div>
    )
}
