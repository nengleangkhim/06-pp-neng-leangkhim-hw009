import React from 'react'
import { Navbar, Brand, Link, Nav, Form, Button, FormControl, Container } from 'react-bootstrap';

export default function NavbarLink() {
    return (
        <div>
            <>
            <Navbar bg="dark" variant="dark">
                <Container>
                    <Navbar.Brand href="/home">React-Router</Navbar.Brand>
                    <Nav className="mr-auto">
                        <Nav.Link href="/home">Home</Nav.Link>
                        <Nav.Link href="/video">Video</Nav.Link>
                        <Nav.Link href="/account">Account</Nav.Link>
                        <Nav.Link href="#">Welcome</Nav.Link>
                        <Nav.Link href="/auth">Auth</Nav.Link>
                    </Nav>
                    <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                        <Button variant="outline-info">Search</Button>
                    </Form>
                </Container>
            </Navbar>            
            </>
        </div>
    )
}
