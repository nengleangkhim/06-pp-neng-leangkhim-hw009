import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useLocation
} from "react-router-dom";
import { Button  } from 'react-bootstrap';
import React from "react";

export default function QueryParamsExample() {
  return (
    <Router>
      <QueryParamsDemo />
    </Router>
  );
}

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function QueryParamsDemo() {
    let query = useQuery();

  return (
    <div>
        <Router>
        <div>
            <h1>Video</h1>
                <Button variant="secondary" size="md" >                
                    <Link to="/movie" className="movie">Movie</Link>
                </Button>
                <Button variant="secondary" size="md" >                
                    <Link to="/animation" className="anima">Animation</Link>
                </Button>
            <Switch>
            <Route exact path="/movie">
                <Movie />
                
            </Route>
            <Route path="/animation">
                <Animation />             
            </Route>
            </Switch>
        </div>
        </Router>     
        <Movie/>
        <Animation/>
        <Child name={query.get("name")} />
    </div>
  );
  function Movie() {
    return (
      <div>
        <h2>Movie Catagory</h2>
          <Button variant="secondary" size="md">                
              <Link to="/account?name=Adventure" className="movie">Adventure</Link>
          </Button>
          <Button variant="secondary" size="md" >                
              <Link to="/account?name=Crime" className="anima">Crime</Link>
          </Button>
          <Button variant="secondary" size="md" >                
               <Link to="/account?name=Action" className="movie">Action</Link>
          </Button>
          <Button variant="secondary" size="md">                
              <Link to="/account?name=Romance" className="anima">Romance</Link>
          </Button>
          <Button variant="secondary" size="md" >                
              <Link to="/account?name=Comedy" className="anima">Comedy</Link>
          </Button>
      </div>
      
    );
  }
}

  function Animation() {
    return (
      <div>
        <h2>Animation Catagory</h2>
          <Button variant="secondary" size="md" >                
              <Link to="/account?name=Action" className="movie">Action</Link>
          </Button>
          <Button variant="secondary" size="md" >                
              <Link to="/account?name=Romance" className="anima">Romance</Link>
          </Button>
          <Button variant="secondary" size="md">                
              <Link to="/account?name=Comedy" className="anima">Comedy</Link>
          </Button>
          
      </div>
    );
  }

function Child({ name }) {
  return (
    <div>
      {name ? (
        <h3>
          Pleaes Choose Category : <code> {name}</code>
          
        </h3>
        ) : (
        <h3>There is no name in the query string</h3>
      )}
    </div>
  );
}

