import React from 'react'
import { Card, Img, Title, Text, Button, Body, Row, Col  } from 'react-bootstrap';
import {Link, Router, Switch, Route, useLocation  } from "react-router-dom";


export default function home() {
    return (
        <div>  
                
                <Row>                
                    <Col>
                    <br/>
                        <Card style={{ width: '18rem' }}>
                            <Card.Img variant="top" src="Image/seven.jfif" />
                            <Card.Body>
                                <Card.Title>Seven Deadly Sins</Card.Title>
                                <Card.Text>
                                    Content
                                </Card.Text>
                                <Button variant="primary">
                                    <Link to="/detail1" className="movie">Read</Link>
                                </Button>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col>
                    <br/>
                        <Card style={{ width: '18rem' }}>
                            <Card.Img variant="top" src="Image/demon.jfif" />
                            <Card.Body>
                                <Card.Title>Demon Slayer</Card.Title>
                                <Card.Text>
                                    Content
                                </Card.Text>
                                <Button variant="primary" >
                                    <Link to="/detail2" className="movie">Read</Link>
                                </Button>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col>
                    <br/>
                        <Card style={{ width: '18rem' }}>
                            <Card.Img variant="top" src="Image/sword.jpg" />
                            <Card.Body>
                                <Card.Title>Sword Art Online</Card.Title>
                                <Card.Text>
                                    Content
                                </Card.Text>
                                <Button variant="primary" >
                                    <Link to="/detail3" className="movie">Read</Link>
                                </Button>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col>
                    <br/>
                        <Card style={{ width: '18rem' }}>
                            <Card.Img variant="top" src="Image/sevens.jfif" />
                            <Card.Body>
                                <Card.Title>Seven Deadly Sins</Card.Title>
                                <Card.Text>
                                    Content
                                </Card.Text>
                                <Button variant="primary" >
                                    <Link to="/detail4" className="movie">Read</Link>
                                </Button>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col>
                    <br/>
                        <Card style={{ width: '18rem' }}>
                            <Card.Img variant="top" src="Image/juju.jfif" />
                            <Card.Body>
                                <Card.Title>Jujutsu Kaisen</Card.Title>
                                <Card.Text>
                                    Content
                                </Card.Text>
                                <Button variant="primary" >
                                    <Link to="/detail5" className="movie">Read</Link>
                                </Button>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col>
                    <br/>
                        <Card style={{ width: '18rem' }}>
                            <Card.Img variant="top" src="Image/one.jfif" />
                            <Card.Body>
                                <Card.Title>One Punch</Card.Title>
                                <Card.Text>
                                    Content
                                </Card.Text>
                                <Button variant="primary" >
                                    <Link to="/detail6" className="movie">Read</Link>
                                </Button>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>     
                {/* <Router>            
                <Switch>
                    <Route path="/detail1">
                        <Detail1/>
                    </Route>
                </Switch>
                </Router> */}
        </div>

    )
    

 }
// function Detail1() {
//     return (
//       <div>
//         <h4>Deatil 1</h4>
//       </div>
      
//     );
// }
// function Detail2() {
//     return (
//       <div>
//         <h4>Deatil 2</h4>
//       </div>
      
//     );
// }
// function Detail3() {
//     return (
//       <div>
//         <h4>Deatil 3</h4>
//       </div>
      
//     );
// }
// function Detail4() {
//     return (
//       <div>
//         <h4>Deatil 4</h4>
//       </div>
      
//     );
// }
// function Detail5() {
//     return (
//       <div>
//         <h4>Deatil 5</h4>
//       </div>
      
//     );
// }
// function Detail6() {
//     return (
//       <div>
//         <h4>Deatil 6</h4>
//       </div>
      
//     );
// }